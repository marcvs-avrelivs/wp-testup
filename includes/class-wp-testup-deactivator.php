<?php

/**
 * Fired during plugin deactivation
 *
 * @link       22
 * @since      1.0.0
 *
 * @package    Wp_Testup
 * @subpackage Wp_Testup/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Testup
 * @subpackage Wp_Testup/includes
 * @author     gar war <gary@guardian-designs.com>
 */
class Wp_Testup_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
