<?php

/**
 * Fired during plugin activation
 *
 * @link       22
 * @since      1.0.0
 *
 * @package    Wp_Testup
 * @subpackage Wp_Testup/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Testup
 * @subpackage Wp_Testup/includes
 * @author     gar war <gary@guardian-designs.com>
 */
class Wp_Testup_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
