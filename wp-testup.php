<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              22
 * @since             1.0.0
 * @package           Wp_Testup
 *
 * @wordpress-plugin
 * Plugin Name:       WP-TestUp
 * Plugin URI:        11
 * BitBucket Plugin URI:  https://bitbucket.org/marcvs-avrelivs/wp-testup
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           49.64.81
 * Author:            gar war
 * Author URI:        22
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-testup
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WP_TESTUP_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-testup-activator.php
 */
function activate_wp_testup() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-testup-activator.php';
	Wp_Testup_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-testup-deactivator.php
 */
function deactivate_wp_testup() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-testup-deactivator.php';
	Wp_Testup_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_testup' );
register_deactivation_hook( __FILE__, 'deactivate_wp_testup' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-testup.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_testup() {

	$plugin = new Wp_Testup();
	$plugin->run();

}
run_wp_testup();
